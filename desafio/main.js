var x = "Hello World"
var count;
// Primeira
for (count = 0; count < x.length; count++) {
    if (count == 0) {
        document.write("<p>" + x[count])
    } else {
        document.write(x[count]);
    }
}

// Segundo
for (count = 0; count < x.length; count++) {
    if (count == 0) {
        document.write("<p>")
    }
    document.write(x[count] + " ");
}

// Terceiro
for (count = (x.length - 1); count >= 0; count--) {
    if (count == x.length - 1) {
        document.write("<p>" + x[count]);
    } else {
        document.write(x[count]);
    }
}

// Quarto
for (count = 0; count < x.length; count++) {
    if (count == 0) {
        document.write("<p style='transform: scale(-1, 1)'>" + x[count]);
    } else {
        document.write(x[count]);
    }
}

// Quinto
for (count = 0; count < x.length; count++) {
    if (count == 0) {
        document.write("<p style='transform: scale(1, -1)'>" + x[count]);
    } else {
        document.write(x[count]);
    }
}

// Sexto
for (count = 0; count < x.length; count++) {
    if (count == 0) {
        document.write("<p>" + x[count].toLowerCase());
    } else if (count == x.length - 1) {
        document.write(x[count].toLowerCase());
    } else {
        document.write(x[count].toLowerCase());
    }
}

// Sétimo
for (count = 0; count < x.length; count++) {
    if (count == 0) {
        document.write("<p>" + x[count].toUpperCase());
    } else {
        document.write(x[count].toUpperCase());
    }
}

// Oitavo
for (count = 0; count < x.length; count++) {
    if (count == 0) {
        document.write("<p>" + x[count].toUpperCase() + "<br>");
    } else {
        document.write(x[count].toUpperCase() + "<br>");
    }
}

// Nona
for (count = (x.length - 1); count >= 0; count--) {
    if (count == x.length - 1) {
        document.write("<p>" + x[count]);
    } else {
        document.write("<br>" + x[count]);
    }
}

// Décimo
for (count = (x.length - 1); count >= 0; count--) {
    if (count == x.length - 1) {
        document.write("<p>" + x[count].toUpperCase() + " ");
    } else {
        document.write(x[count].toUpperCase() + " ");
    }
}